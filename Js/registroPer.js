document.addEventListener("DOMContentLoaded", function () {
    const registroLink = document.getElementById("registro-link");
    const loginLink = document.getElementById("login-link");
    const registroForm = document.getElementById("registro-form");
    const loginForm = document.querySelector(".login");
  
    registroLink.addEventListener("click", function (event) {
      event.preventDefault();
  
      // Oculta el formulario de inicio de sesión
      loginForm.style.display = "none";
  
      // Muestra el formulario de registro
      registroForm.style.display = "block";
    });
  
    loginLink.addEventListener("click", function (event) {
      event.preventDefault();
  
      // Oculta el formulario de registro
      registroForm.style.display = "none";
  
      // Muestra el formulario de inicio de sesión
      loginForm.style.display = "block";
    });
  });
  